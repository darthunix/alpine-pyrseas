# Alpine Pyrseas #

Alpine Perseas is a lightweight docker container with Pyrseas tools for PostgreSQL database version control based on Alpine linux.
This container uses [Pyrseas repository](https://github.com/perseas/Pyrseas) on github and offician [python:3-alpine](https://hub.docker.com/r/library/python/tags/) docker container from Docker Hub.

## Usage ##

```
docker run darthunix/pyrseas dbtoyaml -H <db_hostname> -U <db_username> <db_name> > <your_yaml_file>
docker run --mount type=bind,source=<your_yaml_file>,target=/schema.yaml darthunix/pyrseas yamltodb -H <db_hostname> -U <db_username> <db_name> /schema.yaml
```
